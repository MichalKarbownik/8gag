package com.example.a8GAG.MainActivity

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.example.a8GAG.AddPictureActivity
import com.example.a8GAG.R
import com.example.a8GAG.helpers.MainActivitySwipeToDeleteCallback
import com.example.a8GAG.helpers.SerializationHelpers
import com.example.a8GAG.logic.PictureData
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {
    private val ADD_PICTURE_REQUEST = 1
    private val pictureDataList = LinkedList<PictureData>()

    private lateinit var recyclerView: RecyclerView
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = MainActivityAdapter(this.pictureDataList)
        val linearLayoutManager = LinearLayoutManager(this)
        val itemTouchHelper = ItemTouchHelper(MainActivitySwipeToDeleteCallback(adapter))

        this.recyclerView = gag_main_recycler_view
        this.linearLayoutManager = linearLayoutManager

        this.recyclerView.layoutManager = this.linearLayoutManager
        this.recyclerView.adapter = adapter

        itemTouchHelper.attachToRecyclerView(this.recyclerView)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.gag_main_options_menu_add -> {
                startAddPictureActivity()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == ADD_PICTURE_REQUEST && resultCode == Activity.RESULT_OK) {
            val pictureDataJson = data!!.getStringExtra(AddPictureActivity.EXTRA_PICTURE_DATA)
            val pictureData = SerializationHelpers.gson.fromJson(pictureDataJson, PictureData::class.java)

            pictureDataList.add(pictureData)
        }
    }

    private fun startAddPictureActivity() {
        val intent = Intent(this, AddPictureActivity::class.java)
        startActivityForResult(intent, ADD_PICTURE_REQUEST)
    }
}
