package com.example.a8GAG.MainActivity

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.a8GAG.R
import com.example.a8GAG.logic.PictureData
import com.squareup.picasso.Picasso
import java.lang.Exception
import java.util.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.Bitmap
import com.example.a8GAG.PictureDetails.Constants
import com.example.a8GAG.PictureDetails.PictureDetailsActivity
import com.example.a8GAG.helpers.SerializationHelpers
import com.example.a8GAG.helpers.TaggingHelper
import kotlinx.android.synthetic.main.picture_card.view.*

class MainActivityAdapter(private val pictureList: LinkedList<PictureData>): RecyclerView.Adapter<MainActivityAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.picture_card, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val picture = pictureList[holder.adapterPosition]

        holder.title.text = picture.title
        holder.date.text = picture.date

        Picasso.get().load(picture.url)
            .error(R.mipmap.ic_launcher)
            .into(holder.picture, object : com.squareup.picasso.Callback {
                override fun onError(e: Exception?) {
                    Toast.makeText(holder.picture.context, e.toString(), Toast.LENGTH_LONG).show()
                }

                override fun onSuccess() {
                    val pictureBitmap: Bitmap = (holder.picture.drawable as BitmapDrawable).bitmap

                    TaggingHelper.putTagsInTextView(pictureBitmap, holder.tags, picture)

                    holder.itemView.setOnClickListener {
                        openPictureDetailsActivity(holder)
                    }
                }
            })
    }

    override fun getItemCount(): Int = pictureList.size

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.gag_main_picture_title
        val date: TextView = itemView.gag_main_picture_date
        val picture: ImageView = itemView.gag_main_picture
        val tags: TextView = itemView.gag_main_picture_tags
    }

    fun removeItemAt(position: Int) {
        pictureList.removeAt(position)
        notifyItemRemoved(position)
    }

    fun openPictureDetailsActivity(holder: ViewHolder) {
        val intent = Intent(holder.picture.context, PictureDetailsActivity::class.java)
        val json = SerializationHelpers.gson.toJson(pictureList)

        intent.putExtra(Constants.PICTURE_LIST, json)
        intent.putExtra(Constants.PICTURE_POSITION, holder.adapterPosition)

        holder.picture.context.startActivity(intent)
    }
}