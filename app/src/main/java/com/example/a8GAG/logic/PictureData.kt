package com.example.a8GAG.logic

data class PictureData(val url: String, val title: String, val date: String, var tags: List<String> = listOf(""))