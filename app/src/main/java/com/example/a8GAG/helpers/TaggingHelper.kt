package com.example.a8GAG.helpers

import android.graphics.Bitmap
import android.widget.TextView
import com.example.a8GAG.logic.PictureData
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage

class TaggingHelper {
    companion object {
        private const val NUMBER_OF_TAGS = 3

        fun putTagsInTextView(picture: Bitmap, textView: TextView, pictureData: PictureData) {
            val vision = FirebaseVisionImage.fromBitmap(picture)
            val labeler = FirebaseVision.getInstance().onDeviceImageLabeler
            labeler.processImage(vision)
                .addOnSuccessListener { labels ->
                    pictureData.tags = labels.map { it.text }
                    textView.text = (labels.map { it.text }.take(NUMBER_OF_TAGS).joinToString ( ", " ))
                }
        }

        fun giveTagsSimilarityLevel(originalTags: List<String>, tagsToCompare: List<String>): Int {
            var similarityLevel = 0

            originalTags.forEachIndexed { originalTagIndex, originalTag ->
                for(tagToCompareIndex in originalTagIndex until tagsToCompare.size) {
                    if(originalTag == tagsToCompare[tagToCompareIndex]) {
                        similarityLevel++
                    }
                }
            }

            return similarityLevel
        }
    }
}