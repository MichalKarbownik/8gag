package com.example.a8GAG.helpers

import com.example.a8GAG.logic.PictureData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*

class SerializationHelpers {
    companion object {
        val gson = Gson()

        fun pictureListFromJson(pictureListJson: String?): LinkedList<PictureData> {
            val type: Type? = object : TypeToken<LinkedList<PictureData>>() {}.type
            return this.gson.fromJson(pictureListJson, type)
        }
    }
}