package com.example.a8GAG

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.EditText
import com.example.a8GAG.helpers.SerializationHelpers
import com.example.a8GAG.logic.PictureData
import kotlinx.android.synthetic.main.activity_add_picture.*
import java.text.DateFormat
import java.util.*

class AddPictureActivity : AppCompatActivity() {
    private lateinit var editTextUrl: EditText
    private lateinit var editTextTitle: EditText

    companion object {
        const val EXTRA_PICTURE_DATA = "com.example.a8GAG.EXTRA_PICTURE_DATA"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_picture)

        editTextUrl = gag_add_picture_edit_text_url
        editTextTitle = gag_add_picture_edit_text_title

        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_action_close)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle(R.string.gag_add_picture_action_bar_title)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_add_picture, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.gag_add_picture_options_menu_save -> {
                savePicture()
                true
            }
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun savePicture() {
        val url = editTextUrl.text.toString()
        val title = editTextTitle.text.toString()
        val date = DateFormat.getDateTimeInstance().format(Date())
        val data = Intent()

        if(url.isEmpty()) {
            setResult(RESULT_CANCELED, data)
            finish()
        }
        else {
            val pictureData = SerializationHelpers.gson.toJson(PictureData(url, title, date))

            data.putExtra(EXTRA_PICTURE_DATA, pictureData)
            setResult(RESULT_OK, data)
            finish()
        }
    }
}
