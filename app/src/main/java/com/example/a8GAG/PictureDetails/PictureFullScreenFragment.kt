package com.example.a8GAG.PictureDetails

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.example.a8GAG.R
import com.example.a8GAG.helpers.SerializationHelpers
import com.example.a8GAG.logic.PictureData
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_picture_full_screen.view.*
import java.util.*

class PictureFullScreenFragment: Fragment() {
    private lateinit var imageView: ImageView
    private lateinit var pictureList: LinkedList<PictureData>
    private lateinit var picture: PictureData

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_picture_full_screen, container, false)
        this.imageView = view.gag_picture_details_fullscreen_image_view

        val pictureListJson = arguments?.getString(Constants.PICTURE_LIST)
        this.pictureList = SerializationHelpers.pictureListFromJson(pictureListJson)

        val picturePosition = arguments?.getInt(Constants.PICTURE_POSITION)!!
        this.picture = pictureList[picturePosition]

        Picasso.get().load(this.picture.url)
            .error(R.mipmap.ic_launcher)
            .into(this.imageView, object : com.squareup.picasso.Callback {
                override fun onError(e: Exception?) {
                    Toast.makeText(imageView.context, e.toString(), Toast.LENGTH_LONG).show()
                }

                override fun onSuccess() {
                    imageView.setOnClickListener {
                        val pictureDataFragment = PictureDataFragment()
                        val similarPicturesFragment = SimilarPicturesFragment()
                        val pictureFullScreenFragment = fragmentManager?.findFragmentByTag(Constants.PICTURE_FULL_SCREEN_FRAGMENT)!!
                        val transaction = fragmentManager!!.beginTransaction()

                        pictureDataFragment.arguments = arguments
                        similarPicturesFragment.arguments = arguments

                        transaction.hide(pictureFullScreenFragment)
                        transaction.add(R.id.gag_picture_details_linear_layout, pictureDataFragment)
                        transaction.add(R.id.gag_picture_details_linear_layout, similarPicturesFragment)
                        transaction.addToBackStack(null)
                        transaction.commit()
                    }
                }
            })

        return view
    }
}