package com.example.a8GAG.PictureDetails

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.a8GAG.R
import com.example.a8GAG.helpers.SerializationHelpers
import com.example.a8GAG.logic.PictureData
import kotlinx.android.synthetic.main.fragment_picture_data.view.*
import java.util.*

class PictureDataFragment: Fragment() {
    private lateinit var pictureList: LinkedList<PictureData>
    private lateinit var picture: PictureData
    private lateinit var pictureTitleTextView: TextView
    private lateinit var pictureDateTextView: TextView
    private lateinit var pictureTagsTextView: TextView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_picture_data, container, false)
        this.pictureTitleTextView = view.gag_picture_details_title
        this.pictureDateTextView = view.gag_picture_details_date
        this.pictureTagsTextView = view.gag_picture_details_tags

        val pictureListJson = arguments?.getString(Constants.PICTURE_LIST)
        val picturePosition = arguments?.getInt(Constants.PICTURE_POSITION)!!
        this.pictureList = SerializationHelpers.pictureListFromJson(pictureListJson)
        this.picture = pictureList[picturePosition]

        this.pictureTitleTextView.text = this.picture.title
        this.pictureDateTextView.text = this.picture.date
        this.pictureTagsTextView.text = this.picture.tags.joinToString(", ") { it }

        return view
    }
}