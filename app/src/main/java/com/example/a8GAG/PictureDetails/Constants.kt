package com.example.a8GAG.PictureDetails

class Constants {
    companion object {
        const val PICTURE_LIST: String = "gag_pictureDetails_pictureList"
        const val PICTURE_POSITION: String = "gag_pictureDetails_picturePosition"
        const val PICTURE_FULL_SCREEN_FRAGMENT = "gag_pictureDetails_pictureFullScreenFragment"
    }
}