package com.example.a8GAG.PictureDetails


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast

import com.example.a8GAG.R
import com.example.a8GAG.helpers.SerializationHelpers
import com.example.a8GAG.helpers.TaggingHelper
import com.example.a8GAG.logic.PictureData
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_similar_pictures.view.*
import java.util.*

class SimilarPicturesFragment : Fragment() {
    private val MAX_NUMBER_OF_SIMILAR_PICTURES = 6
    private lateinit var pictureList: LinkedList<PictureData>
    private lateinit var picture: PictureData
    private lateinit var similarPicturesCarousel: LinearLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_similar_pictures, container, false)
        this.similarPicturesCarousel = view.gag_fragment_similar_pictures_carousel

        val pictureListJson = arguments?.getString(Constants.PICTURE_LIST)
        val picturePosition = arguments?.getInt(Constants.PICTURE_POSITION)!!
        this.pictureList = SerializationHelpers.pictureListFromJson(pictureListJson)
        this.picture = pictureList[picturePosition]

        val similarPictures: List<PictureData> = getSimilarPictures()

        similarPictures.forEach {
            val imageView = ImageView(view.context)

            Picasso.get().load(it.url)
                .error(R.mipmap.ic_launcher)
                .into(imageView, object : com.squareup.picasso.Callback {
                    override fun onError(e: Exception?) {
                        Toast.makeText(imageView.context, e.toString(), Toast.LENGTH_LONG).show()
                    }

                    override fun onSuccess() {
                        similarPicturesCarousel.addView(imageView)
                    }
                })
        }

        return view
    }

    fun findPicturesSimilarity(): LinkedList<Pair<Int, PictureData>> {
        val pictureSimilarityLevelMap: LinkedList<Pair<Int, PictureData>> = LinkedList()

        this.pictureList.forEach {
            val similarity: Int = TaggingHelper.giveTagsSimilarityLevel(this.picture.tags, it.tags)

            pictureSimilarityLevelMap.add(Pair(similarity, it))
        }

        return pictureSimilarityLevelMap
    }

    fun getSimilarPictures(): List<PictureData> {
        val picturesSimilarityList = findPicturesSimilarity()
        val sortedPicturesSimilarityList = picturesSimilarityList.sortedByDescending { it.first }

        return sortedPicturesSimilarityList.map{ it.second }.take(MAX_NUMBER_OF_SIMILAR_PICTURES)
    }
}