package com.example.a8GAG.PictureDetails

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.a8GAG.R

class PictureDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picture_details)

        val pictureListJson = intent?.extras?.getString(Constants.PICTURE_LIST)
        val picturePosition = intent?.extras?.getInt(Constants.PICTURE_POSITION)!!

        supportActionBar!!.hide()

        val fullScreenFragment = createPictureFullScreenFragment(pictureListJson, picturePosition)

        supportFragmentManager.beginTransaction().add(R.id.gag_picture_details_linear_layout, fullScreenFragment, Constants.PICTURE_FULL_SCREEN_FRAGMENT).commit()
    }

    private fun createPictureFullScreenFragment(pictureListJson: String?, picturePosition: Int): PictureFullScreenFragment {
        val bundle = Bundle()
        bundle.putString(Constants.PICTURE_LIST, pictureListJson)
        bundle.putInt(Constants.PICTURE_POSITION, picturePosition)

        val pictureFullScreenFragment = PictureFullScreenFragment()
        pictureFullScreenFragment.arguments = bundle

        return pictureFullScreenFragment
    }
}

